
Aggregation Block Module
========================
Phil Glatz   1 August, 2010

This module provides a block to display a list of titles of recent articles from 
a feed created by the Aggregation module. A block may be created for each feed 
you have defined. Please note this module will work only with feeds created by 
the Aggregation module.

The block displays a configurable (per feed) number of titles, each linking to 
the original article.

An administration form presents a list of feeds, with a checkbox to enable a
block based on the feed. If the feed is enabled, a block will be created for the
feed. Each enabled feed may be configured to record the number of items to
display in the block, and to have a custom block title.


Installation
------------
The Aggregation module must be installed and enabled.

Extract the module to your sites/all/modules directory and enable it.

Go to admin/content/aggregation_block to enable the feeds you want blocks for. 
You may also specify the number of links to display in the block, and a custom 
title for the block (the default is the feed name).

Go to admin/build/block to enable your feed blocks. For every feed you have 
enabled, there will be a block with the title "Aggregation Feed: ", followed by 
the feed name.

If you uninstall this module, run update.php to remove the small table used from 
your database.


Configuration
-------------
Go to admin/settings/aggregation_block to set configuration options.

Click the box to allow/disallow the display of a "more" link at the bottom of 
each Aggregation Block. This will create a link to the full feed.
