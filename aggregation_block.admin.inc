<?php

/**
 * @file
 * Admin include file.
 */

/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function aggregation_block_admin_settings() {
  $form['aggregation_block_showmore'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display "more" link in blocks.'),
    '#description'   => t('Create link to display full feed page.'),
    '#default_value' => variable_get('aggregation_block_showmore', TRUE),
  );

  // Return the form with the system buttons & action added to it
  return system_settings_form($form);
}

